module "compute" {
  source = "../modules/compute_front"

  instance_name   = "cable-front-prod"
  machine_type    = "e2-standard-2"
  network_name    = module.network.network_name
  region          = var.region
  compute_address = "cable-debian"
  tags            = ["allow-ssh", "http-server", "https-server", "allow-web"]
}
