module "network" {
  source = "../modules/network"

  name                   = "cable-sandbox-network"
  allow_fw_protocol      = "tcp"
  network_ports_list     = ["8080"]
  is_public_access       = true
  private_fw_source_tags = ["internal-instance-tag"]
  private_fw_target_tags = ["internal-instance-tag"]
  region                 = var.region

  # ensure ip addresses are identical (can automate later...)
  private_fw_source_ranges = ["10.0.1.0/24"]
  ip_cidr_range            = "10.0.1.0/24"

}
