module "elb" {
    
  source = "../modules/lb"

  front_instance_self_linked = module.compute.front_instance_self_linked

}
