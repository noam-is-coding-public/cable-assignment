module "compute_api" {
  source = "../modules/compute_api"

  region             = var.region
  api_a              = "cable-api-prod-a"
  api_a_machine_type = "e2-standard-2"
  api_b              = "cable-api-prod-b"
  api_b_machine_type = "e2-standard-2"
  private_subnet     = module.network.private_subnet
  firewall_tags      = module.network.private_fw_target_tags
  sql_instance       = module.data.sql_instance

}
