module "data" {
  source = "../modules/data"

  sql_instance_name = "cable-mysql-instance-prod"
  mysql_version     = "MYSQL_5_7"
  region            = var.region
  sql_machine_type  = "db-f1-micro"
  private_network   = module.network.network_self_link

}
