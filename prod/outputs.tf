# output the testing ip
output "static_ip_front" {
  value = module.compute.static_ip_front
}

# output the sql instance details to then pass to the app
output "sql_instance_data" {
  value     = module.data.sql_instance
  sensitive = true
}
