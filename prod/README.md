# prod
main entry for creating the prod env
public access to the React app (`front`) via port `8080`
private access to 2 instances running Go API
private access to an instance of MySQL

## how?
- set the project
- enable all the apis (I didn't automate it and my env was already sorted)
- set the tf backend (S3 bucket, really)
- get the auth key and refer to it
```
terraform init
terraform plan
# check the plan
terraform apply
# check the plan again and type Yes

# enjoy the env

# finally...
terraform destroy
# check the plan and type Yes
```