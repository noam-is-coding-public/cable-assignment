terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.37.0"
    }
  }
}

# sets the remote backend.
# for the sake of simplicity the backend is created manually.
terraform {
  backend "gcs" {
    bucket = "cable-sandbox-prod"
    prefix = "terraform/state"
  }
}

# sets the project props
provider "google" {
  credentials = file("<PATH_TO_JSON>")

  project = <"PROJECT_NAME">
  region  = "europe-west2"
  zone    = "europe-west2-b"
}

