#! /bin/bash
 #apt update
 #apt -y install apache2
 #cat <<EOF > /var/www/html/index.html
 #<html><body><p>Linux startup script added directly.</p></body></html>
 #EOF

sudo apt update
sudo apt git
sudo apt-get install git -y
sudo git clone https://github.com/aevitas/hello-world-react



# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update


sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

cd hello-world-react/

sudo  docker build -t hello-world-react .
sudo docker run -p 8080:80 hello-world-react
