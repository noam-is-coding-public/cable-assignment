# The Task

Consider an application team that is responsible for deploying the following stack: a React website, communicating with 2 Go APIs, which communicate with each other as well as a SQL database.

The task is to build a Terraform project that will produce the necessary resources to run the application stack as described.

A production and a staging environment will be needed. There should be no public access on staging. Consider specifying in the project load balancing, ingress, dns, certificates and anything else you deem relevant.


## Resources
testing images pulled from here:
https://github.com/aevitas/hello-world-react
https://github.com/codeship/go-hello-world


## Other Considerations and scaling options
- Terragrunt (or equivalent tools) should be considered for a larger team
- K8s is too complex for this setup
- remote state file, ideally, should be manage for each module to create a more loosely coupled system
- writing wrappers to execute the code can help with running modules in a specific order and calling Terraform from within the CD process
- envs should run in different projects; I run both `prod` and `staging` on the same env here
- images in 'real' env will be pulled from an internal registry rather than from github

## How to run?
refer to the `README` file in each env for specific instructions. 
