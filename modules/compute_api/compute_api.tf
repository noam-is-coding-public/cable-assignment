# API A
resource "google_compute_instance" "api_a" {
  name                      = var.api_a
  machine_type              = var.api_a_machine_type
  allow_stopping_for_update = true
  zone                      = "${var.region}-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = var.private_subnet.name
  }

  metadata = {
    enable-oslogin : "TRUE"
  }

  tags = var.firewall_tags

  metadata_startup_script = file("./scripts/api_startup.sh")
}

# API B
resource "google_compute_instance" "api_b" {
  name                      = var.api_b
  machine_type              = var.api_b_machine_type
  allow_stopping_for_update = true
  zone                      = "${var.region}-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = var.private_subnet.name
  }

  metadata = {
    enable-oslogin : "TRUE"
  }

  tags = var.firewall_tags

  # demo only. Ideally after getting to know the app I'd baked in the image, pass the props to the container as env vars and hook up with the other services (etc SQL, SRE etc)
  metadata_startup_script = file("./scripts/api_startup.sh")
}

# useful for testing. No need in a real prod env
#resource "google_compute_address" "api_a_static_ip" {
#  name   = var.api_a
#  region = var.region
#}

# useful for testing. No need in a real prod env
#resource "google_compute_address" "api_b_static_ip" {
#  name   = var.api_b
#  region = var.region
#}
