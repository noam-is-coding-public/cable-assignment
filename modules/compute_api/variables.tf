variable "api_a" {}
variable "api_a_machine_type" {}
variable "api_b" {}
variable "api_b_machine_type" {}
variable "private_subnet" {}
variable "region" {}
variable "firewall_tags" { type = list(string)}

# we're not using it now but we will be when we wire the app with the SQL db
variable "sql_instance" {}