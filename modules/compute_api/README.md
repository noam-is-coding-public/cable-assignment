# API module
create two demo instances for the Go APIs and boot them using a startup script.

## Things I haven't covered/done yet
- test the instances are reachable from the frontend via the API endpoints
- pass the database connection params to the apps (though the db params are available to this module)
- limit access via a Bastion host only
- attach disks
- think about failover, observability and scalability
- define architecture for more than 2 instances
- write `variables` descriptions

