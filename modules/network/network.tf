# a very basic vpc to get started
resource "google_compute_network" "default" {
  name = var.name
}

# for debugging the vm
resource "google_compute_firewall" "allow_ssh" {
  name          = "${var.name}-allow-ssh"
  network       = google_compute_network.default.name
  target_tags   = ["allow-ssh"] # this targets our tagged VM
  source_ranges = ["0.0.0.0/0"] # limit to a Bastion only (which we don't have at the moment)
  source_tags   = ["${var.name}-ssh"]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

# diable public access in staging. 
# the logic here is either to (1) add a bool condition to check if we're on prod and disblae the public access on the instance or (2) block the firewall rule
resource "google_compute_firewall" "allow_web" {
  count       = var.is_public_access ? 1 : 0
  name        = "${var.name}-firewall"
  network     = google_compute_network.default.name
  source_tags = ["${var.name}-web"]
  target_tags = ["allow-web"]

  allow {
    protocol = "icmp" # trap error messages for debugging and analytics
  }

  allow {
    protocol = var.allow_fw_protocol
    ports    = var.network_ports_list
  }

  source_ranges = [
    "0.0.0.0/0"
  ]

}
