output "network_name" {
    value = google_compute_network.default.name
}

output "network_self_link" {
  value = google_compute_network.default.self_link
}

output "private_subnet" {
  value = google_compute_subnetwork.private_subnet
}

output "private_subnet_id" {
  value = google_compute_subnetwork.private_subnet.id
}

output "private_fw_target_tags" {
  value = var.private_fw_target_tags
}