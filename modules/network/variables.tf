variable "name" {}

variable "allow_fw_protocol" {}
variable "network_ports_list" { type = list(string) }
variable "private_fw_source_tags" { type = list(string) }
variable "private_fw_target_tags" { type = list(string) }
variable "private_fw_source_ranges" { type = list(string) }
variable "ip_cidr_range" {}
variable "is_public_access" { default = false}
variable "region" {}
