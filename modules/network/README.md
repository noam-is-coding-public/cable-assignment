# the network module
create a public VPC and a private subnet. Set the firewall rules. Use a flag to enable/disable public access to the vpc.

## Things I haven't covered/done yet
- ip mapping
- think about failover, observability and scalability
- write `variables` descriptions
- test the firewall properly
- isolate FW rules for public/private

