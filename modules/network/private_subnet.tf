# A private subnet within our VPC
resource "google_compute_subnetwork" "private_subnet" {
  name                     = "cable-private-subnet-2"
  ip_cidr_range            = var.ip_cidr_range
  network                  = google_compute_network.default.name
  region                   = var.region
  private_ip_google_access = false # don't ever change this or this subnet won't stay private
}

# Let internal traffic in
resource "google_compute_firewall" "allow_internal" {
  name    = "allow-internal"
  network = google_compute_network.default.name

  allow {
    protocol = "all"
  }

  source_tags   = var.private_fw_source_tags
  target_tags   = var.private_fw_target_tags
  source_ranges = var.private_fw_source_ranges 
}

# an internal ip for the sql instance
resource "google_compute_global_address" "private_ip_address" {
    name          = google_compute_network.default.name
    purpose       = "VPC_PEERING"
    address_type = "INTERNAL"
    prefix_length = 16
    network       = google_compute_network.default.name
}

# enable the connection for the above ip
resource "google_service_networking_connection" "private_vpc_connection" {
    network       = google_compute_network.default.name
    service       = "servicenetworking.googleapis.com"
    reserved_peering_ranges = ["${google_compute_global_address.private_ip_address.name}"]
}
