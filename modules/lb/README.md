# Load Balancer module
create an external load balancer for the React app.

## Things I haven't covered/done yet
- enable ssl and https (but the code that should run afterwards is there)
- think about failover, observability and scalability
- write `variables` descriptions
- I also didn't want to clutter the vars and kept some of them hard coded
- I didn't include any DNS records, again, for simplicity  
- internal LBs for private comms
