# First, create a group in which the instances hosting the apps can be added
resource "google_compute_instance_group" "instance_group_front" {
  name        = "cable-ig-front"
  description = "Terraform test instance group"
  zone        = "europe-west2-a"


  instances = [
    var.front_instance_self_linked
  ]

  # enable this when there's a cert on the server
  #named_port {
  #  name = "https"
  #  port = "443"
  #}

  named_port {
    name = "http"
    port = "8080"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# this is the health check between the instance group and the load balancer backend
resource "google_compute_health_check" "http_health_check_front" {
  name        = "cable-health-check-front"
  description = "TF Health check via http"

  timeout_sec         = 5
  check_interval_sec  = 5
  healthy_threshold   = 2
  unhealthy_threshold = 2

  tcp_health_check {
    port         = 8080
    proxy_header = "NONE"
  }
}

# The terms here may be a little confusing... This is the frontends' (app) backend (an lb component). It takes the healthcheck object and uses it to test that the instance group is 
# reachable. It then manages the https and certs.
# The custom_response_headers section is only useful later, when we know more about the internal APIs
resource "google_compute_backend_service" "front" {
  name                  = "cable-backend-service-front"
  health_checks         = [google_compute_health_check.http_health_check_front.id]
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_name             = "http" #"https" uncomment when we're ready to add an ssl cert

  backend {
    group = google_compute_instance_group.instance_group_front.id
  }

  # context here would depend on the React code; kept the standard in
  custom_request_headers = ["Access-Control-Allow-Credentials: true"]
  custom_response_headers = [
    "Access-Control-Allow-Origin: *",
    "Access-Control-Allow-Headers: accept, accept-language, content-language, content-type, authorization, origin, x-auth-token, x-requested-with, x-xsrf-token, x-socket-id",
    "Access-Control-Allow-Credentials: true"
  ]

  log_config {
    enable      = true
    sample_rate = 1.0
  }
}

# https proxy - uncomment when we have an ssl cert
#resource "google_compute_target_https_proxy" "front" {
#  name    = "cable-https-proxy-front"
#  url_map = google_compute_url_map.front.id
#  ssl_certificates = [var.path_to_ssl]
#}

# http proxy
resource "google_compute_target_http_proxy" "front" {
  name    = "cable-http-proxy-front"
  url_map = google_compute_url_map.front.id
}

# reserved IP address for the frontend
resource "google_compute_global_address" "front" {
  name = "cable-static-ip-front"
}

# frontend forwarding rule - binds the proxy with the ip registered in the DNS (we currently don't have a DNS)
resource "google_compute_global_forwarding_rule" "front" {
  name                  = "cable-forwarding-rule-front"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  #port_range            = "443"
  port_range = "8080"
  #target     = google_compute_target_https_proxy.front.id
  target     = google_compute_target_http_proxy.front.id
  ip_address = google_compute_global_address.front.id
}


# url map - and a very minimal one.
# usually this would get a lot more complex as here we'd define the path of each url in our system
resource "google_compute_url_map" "front" {
  name            = "cable-url-map-front"
  default_service = google_compute_backend_service.front.id

  path_matcher {
    name            = "default-matcher"
    default_service = google_compute_backend_service.front.id
  }
}
