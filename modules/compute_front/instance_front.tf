# instance for hosting the React container
resource "google_compute_instance" "front" {
  name                      = var.instance_name
  machine_type              = var.machine_type
  allow_stopping_for_update = true
  zone                      = "${var.region}-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  # disable this in staging and pass the private subnetwork instead. The logic here is either to (1) add a bool condition to check if we're on prod or (2) block the firewall rule
  network_interface {
    network = var.network_name
    access_config {
      nat_ip = google_compute_address.static_ip_front.address
    }
  }

  metadata = {
    enable-oslogin : "TRUE"
  }

  tags = var.tags

  metadata_startup_script = file("./scripts/front_startup.sh")
}

# useful for testing. No need in a real prod env where an lb is set
resource "google_compute_address" "static_ip_front" {
  name   = var.compute_address
  region = var.region
}
