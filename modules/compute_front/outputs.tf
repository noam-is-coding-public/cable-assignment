output "static_ip_front" {
  value = google_compute_address.static_ip_front.address
}

output "front_instance_self_linked" {
  value = google_compute_instance.front.self_link
}