# Front module
create a single instances for the React app and boot it using a startup script.

## Things I haven't covered/done yet
- test the instance can reach the API instances
- limit access via a Bastion host only
- attach disks
- think about failover, observability and scalability
- define architecture for more than 1 instance
- write `variables` descriptions
- I also didn't explicitly disable the public access in staging but left a comment with details on how to do that