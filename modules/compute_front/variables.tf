variable "instance_name" {}
variable "machine_type" {}
variable "tags" { type = set(string) }
variable "network_name" {}
variable "region" {}
variable "compute_address" {}
