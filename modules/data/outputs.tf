# here we'd want to output all the connection string params and pass them to the apps (as env vars via docker/configmaps)
output "sql_instance" {
    value = google_sql_database_instance.sql_instance
}