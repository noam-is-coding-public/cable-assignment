# private SQL instance
resource "google_sql_database_instance" "sql_instance" {
  name             = var.sql_instance_name
  database_version = var.mysql_version
  region           = var.region
  settings {
    tier = var.sql_machine_type

    ip_configuration {
      ipv4_enabled    = false # keep this disabled to avoid leaks to public
      private_network = var.private_network
    }
  }
}
