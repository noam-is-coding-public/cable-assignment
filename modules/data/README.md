# Data module
create a super simple single RDS (MySQL) instance with private access only

## Things I haven't covered/done yet
test the db can be reached by the API instances
limit access via a Bastion host only
write and run a test schema
think about failover, observability and scalability
define architecture for more than 1 instance
write `variables` descriptions