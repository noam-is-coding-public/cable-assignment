# Staging

ok, I could simply copy-paste the code from the `prod` folder, replace the tags, vars and block external access via the `is_public_access` flag. This would demonstrate the requirement to keep staging internal but it would also bloat the code.

My main goal here is to show how we can use the TF structure to manage multiple envs whilst keeping the code DRY.